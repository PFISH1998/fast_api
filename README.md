# BIKI 交易监控爬虫

环境要求:

- python 3.6 +
- MySQL 5.7
- Redis 5.0.4

建议使用虚拟环境运行

安装依赖

```shell
pip install requirements.txt
```

创建 biki_app 数据库

```sql
CREATE DATABASE IF NOT EXISTS yourdbname DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
```

设置数据库连接 `conf/settings.py`

```python
DATABASE = 'mysql+pymysql://{用户名}:{密码}@localhost:3306/biki_app?charset=utf8'
```

打包前端文件（已经gou如更改前端代码需注意）

进入前端目录
`cd web`

安装依赖
`npm install`

打包静态文件
`npm run build:stage`

运行后台 celery 任务

`celery -A tasks worker --loglevel=info`

`celery -A tasks beat`

其他(重要)

设置 biki.com 上的 API 授权码

设置发送邮件通知的相关信息

目录结构

--apps
主要逻辑代码
-web
前端工程项目