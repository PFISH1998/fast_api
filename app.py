import time
import json

from sqlalchemy.orm import Session
from starlette.middleware.cors import CORSMiddleware
from starlette.staticfiles import StaticFiles
from starlette.requests import Request
from starlette.templating import Jinja2Templates
from fastapi import FastAPI, BackgroundTasks, Depends

from apps.biki_apis import BIKI
from apps.sql_app.models import Coin
from apps.sql_app import curd, models, schemas
from apps.sql_app.database import DBSession, engine, SessionLocal
from apps.sql_app.curd import get_new_coin
from apps.task.biki import check_biki_data

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:9528",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="static/static"), name="static")

templates = Jinja2Templates(directory="static")

def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@app.get('/')
def home(request: Request):
    '''
    首页
    '''
    return templates.TemplateResponse('index.html', {"request": request})


@app.get('/coin/list/')
def list_coin(db: Session = Depends(get_db)):
    result = get_new_coin(db)
    return {
        'data': [i.to_dict() for i in result],
        'message': 'success',
        'code': 20000
    }


@app.get('/coin/search/')
def start_task():
    check_biki_data.delay()
    return {'msg': 'ok'}

