import smtplib
from email.mime.text import MIMEText
from email.header import Header
from conf.settings import MAIL


def send_email(subject='BIKI爬虫通知', content='', to_list=[]):
    message = MIMEText(content, 'html', 'utf-8')
    message['From'] = Header('BIKIAPP', 'utf-8')
    message['Subject'] = Header(subject, 'utf-8')
    smtpObj = smtplib.SMTP_SSL()
    smtpObj.connect(MAIL['HOST'], 465)
    smtpObj.login(MAIL['USER'], MAIL['CODE'])
    for receiver in to_list:
        message['To'] = Header(receiver, 'utf-8')
        try:
            smtpObj.sendmail(MAIL['USER'], receiver, message.as_string())
            print("邮件发送成功")
        except smtplib.SMTPException:
            print("Error: 无法发送邮件")
