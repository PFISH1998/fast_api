from conf import settings
from apps.biki_apis import BIKI
from apps.notify.mail import send_email
from apps.notify.mail_tpl import html
from tasks import celery


# 待完善
class Notify:
    def __init__(self):
        self.recivers = []

    def add_reciver(self, reciver):
        self.recivers.append(reciver)

    def send_notify(self):
        for reciver in self.recivers:
            reciver.send()


class BaseReciver(object):

    def send(self):
        pass


class BIKIReciver(BaseReciver):
    def send(self, coin: dict):
        try:
            result = BIKI.get_ticker(coin['symbol'])
            symbol = coin['base_coin'] + '_' + coin['count_coin']
            result.update({'symbol': symbol})
            # 生成邮件内容
            content = html.format(**result)
            send_email(content=content, to_list=settings.MAIL['RECEIVERS'])
        except Exception as e:
            print(e)


@celery.task
def send_notify(coin: dict):
    try:
        result = BIKI.get_ticker(coin['symbol'])
        symbol = coin['base_coin'] + '_' + coin['count_coin']
        result.update({'symbol': symbol})
        # 生成邮件内容
        content = html.format(**result)
        send_email(content=content, to_list=settings.MAIL['RECEIVERS'])
    except Exception as e:
        print(e)
