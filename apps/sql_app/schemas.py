from pydantic import BaseModel

class BIKICoin(BaseModel):
    symbol: str
    count_coin: str
    amount_precision: int
    base_coin: str
    price_precision: int