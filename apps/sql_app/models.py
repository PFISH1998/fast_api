from datetime import datetime

from sqlalchemy import Column, String, Integer, DateTime, func, Float
from .database import Base, engine


class Coin(Base):
    __tablename__ = 'Coin'
    symbol = Column(String(64), primary_key=True)
    base_coin = Column(String(64))
    count_coin = Column(String(64))
    amount_precision = Column(Integer)
    price_precision = Column(Integer)
    vol = Column(String(64))
    high = Column(String(64))
    last = Column(Float)
    low = Column(String(64))
    change = Column(String(64))
    rose = Column(String(64))
    add_at = Column(DateTime, default=datetime.now())
    update_at = Column(DateTime, default=func.now(), onupdate=func.now())

    def to_dict(self):
        d = {}
        for c in self.__table__.columns:
            v = getattr(self, c.name, None)
            if not isinstance(v, datetime):
                d.update({c.name: v})
            else:
                d.update({c.name: v.strftime('%Y-%m-%d %H:%M:%S')})
        return d


Base.metadata.create_all(bind=engine)
