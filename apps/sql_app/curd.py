from sqlalchemy.orm import Session
from .models import Coin
from apps.notify.notify import send_notify


def create_or_update_coin(db: Session, coin: dict):
    """
    更新交易对信息
    """
    db_coin = db.query(Coin).filter(Coin.symbol == coin['symbol'])
    if db_coin.first():
        db_coin.update({
            'base_coin': coin['base_coin'],
            'count_coin': coin['count_coin'],
            'amount_precision': coin['amount_precision'],
            'price_precision': coin['price_precision']
        })
        db.commit()
    else:
        db_coin = Coin(
            base_coin=coin['base_coin'],
            count_coin=coin['count_coin'],
            symbol=coin['symbol'],
            amount_precision=coin['amount_precision'],
            price_precision=coin['price_precision']
        )
        db.add(db_coin)
        db.commit()
        db.refresh(db_coin)
        # send email
        send_notify.delay(coin)
    return True


def get_new_coin(db: Session):
    """
    获取所有交易对数据
    """
    coin = db.query(Coin).filter().order_by(-Coin.add_at)
    return coin


def update_ticker(db: Session, tickers: dict):
    """
    更新交易对详情
    """
    for ticker in tickers:
        db.query(Coin).filter(Coin.symbol == ticker['symbol']).update({
            'vol': ticker['vol'],
            'high': ticker['high'],
            'last': ticker['last'],
            'low': ticker['low'],
            'change': ticker['change'],
            'rose': ticker['rose']
        })
    db.commit()
