from celery.task import periodic_task
from apps.biki_apis import BIKI
from apps.sql_app import curd
from apps.sql_app.database import SessionLocal


@periodic_task(run_every=120)
def check_biki_data():
    """
    定时任务检查交易所，根据数据库比对
    """
    try:
        db = SessionLocal()
        result = BIKI.get_symbols()
        for i in result:
            curd.create_or_update_coin(db, coin=i)
        ticker = BIKI.get_all_ticker()
        # 更新交易对详细信息
        curd.update_ticker(db, ticker)
    except Exception as e:
        print(e)
    finally:
        db.close()
