from apps.biki import rest_api as restapi
from conf.settings import API_KEY, SECRET_KEY


class _BIKI:
    def __init__(self):
        self._API = restapi.RestAPI(API_KEY, SECRET_KEY)

    def get_symbols(self):
        result = self._API.get_symbols()
        if result.get('code', '') == '0':
            return result['data']
        else:
            raise Exception('get symbols fail: {}'.format(result))

    def get_all_ticker(self):
        result = self._API.get_all_ticker()
        if result.get('code', '') == '0':
            return result['data']['ticker']
        else:
            raise Exception('get_all_ticker fail: {}'.format(result))

    def get_ticker(self, symbol):
        result = self._API.get_ticker(symbol)
        if result.get('code', '') == '0':
            return result['data']
        else:
            raise Exception('get_ticker fail: {}'.format(result))


BIKI = _BIKI()
