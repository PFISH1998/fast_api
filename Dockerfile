FROM python:3.6

EXPOSE 80

COPY ./app/requirements.txt /app/

RUN pip install -r app/requirements.txt

WORKDIR /app

CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "80"]