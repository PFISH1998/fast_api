from fastapi import FastAPI, Header, Depends, Cookie
from pydantic import BaseModel

app = FastAPI()

class Item(BaseModel):
    name: str
    price: float

items = {
    'foo': {'name': 'foo', 'price': 2.34},
    'bar': {'name': 'bar', 'price': 23.4}
}

@app.get('/')
def home():
    return {'data': 'Hello world'}

# @app.get('/items/{item_id}')
# def read_item(item_id: int, params: str = None):
#     return {'item_id': item_id, 'params': params}


def commons_params(q: str = None, b: int = 1):
    print(q, b)
    return {'q': q, 'b': b}

@app.get('/items/{item_id}/')
def save_item(com: dict = Depends(commons_params)):
    return {'data': com}