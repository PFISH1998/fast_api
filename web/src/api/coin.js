import request from '@/utils/request'

export function getCoinList() {
  return request({
    url: '/coin/list',
    method: 'get'
  })
}

